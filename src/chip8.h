#ifndef __CHIP8_H__
#define __CHIP8_H__

#define CHIP8_CYCLES_PER_FRAME 15
#define CHIP8_FRAMES_PER_SECOND 60

#define CHIP8_OPCODE_A(opcode) ((opcode & 0xf000) >> 12)
#define CHIP8_OPCODE_X(opcode) ((opcode & 0x0f00) >> 8)
#define CHIP8_OPCODE_Y(opcode) ((opcode & 0x00f0) >> 4)
#define CHIP8_OPCODE_N(opcode) (opcode & 0x000f)
#define CHIP8_OPCODE_NN(opcode) (opcode & 0x00ff)
#define CHIP8_OPCODE_NNN(opcode) (opcode & 0x0fff)

#define CHIP8_RAM_SIZE 0x1000
#define CHIP8_LOAD_ADDRESS 0x0200
#define CHIP8_STACK_SIZE 16

#define CHIP8_SCREEN_WIDTH 64
#define CHIP8_SCREEN_HEIGHT 32
#define CHIP8_VRAM_SIZE CHIP8_SCREEN_WIDTH * CHIP8_SCREEN_HEIGHT

#define CHIP8_FONT_ROM_ADDRESS 0x000
#define CHIP8_FONT_CHARACTER_HEIGHT 5

#define CHIP8_SCREEN_SCALE 8

#include <stdbool.h>
#include <stdint.h>

#include <SDL2/SDL.h>

typedef struct {
    // Registers
    uint8_t v[16];
    uint16_t i;
    uint16_t pc;
    uint8_t sp;
    uint8_t dt;
    uint8_t st;

    // Keyboard
    bool keyboard[16];

    // Memory
    uint8_t ram[4096];

    // Stack
    uint16_t stack[16];

    // Video RAM
    bool vram[2048];
} Chip8;

extern SDL_Scancode Chip8_keymap[16];

void Chip8_init(Chip8* chip8);
void Chip8_frame(Chip8* chip8);
void Chip8_cycle(Chip8* chip8);

#endif
