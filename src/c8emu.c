#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <SDL2/SDL.h>

#include "chip8.h"

int main(int argc, char* argv[]) {
    // Check that we have 1 argument
    if(argc != 2) {
        printf("Usage:\n");
        printf("%s <Chip8 ROM file>\n", argv[0]);
        return 0;
    }

    // Create our Chip8
    Chip8 chip8;

    // Initialize it
    Chip8_init(&chip8);

    // Open the ROM file
    FILE* romFile = fopen(argv[1], "r");

    if(romFile == NULL) {
        fprintf(stderr, "Failed to open ROM file.\n");
        return 1;
    }

    int currentByte = 0;
    
    // Read ROM file
    while(currentByte < CHIP8_RAM_SIZE - CHIP8_LOAD_ADDRESS) {
        int readByte = fgetc(romFile);

        if(readByte == EOF) {
            break;
        } else {
            chip8.ram[currentByte + CHIP8_LOAD_ADDRESS] = readByte;
            currentByte++;
        }
    }

    // Close ROM file
    fclose(romFile);

    printf("Successfully read %d bytes from ROM file.\n", currentByte);

    // Initialize randomizer
    srand(time(NULL));

    // SDL initialization
    if(SDL_Init(SDL_INIT_VIDEO)) {
        SDL_Quit();
        fprintf(stderr, "Error: SDL initialization failed.\n");
        return 1;
    }

    SDL_Window* window = SDL_CreateWindow("c8emu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, CHIP8_SCREEN_SCALE * 64, CHIP8_SCREEN_SCALE * 32, SDL_WINDOW_SHOWN);

    SDL_Event event;
    SDL_Rect position;

    position.w = CHIP8_SCREEN_SCALE;
    position.h = CHIP8_SCREEN_SCALE;

    Uint32 color_black = 0xff000000;
    Uint32 color_white = 0xffffffff;

    // Emulate
    bool emulate = true;

    while(emulate) {
        // Check for events
        while(SDL_PollEvent(&event)) {
            switch(event.type) {
                case SDL_QUIT:
                emulate = false;
                break;

                case SDL_KEYDOWN:
                for(int i = 0; i < 16; i++) {
                    if(event.key.keysym.scancode == Chip8_keymap[i]) {
                        chip8.keyboard[i] = true;
                        break;
                    }
                }
                break;

                case SDL_KEYUP:
                for(int i = 0; i < 16; i++) {
                    if(event.key.keysym.scancode == Chip8_keymap[i]) {
                        chip8.keyboard[i] = false;
                        break;
                    }
                }
                break;
            }
        }

        // Draw screen
        for(int y = 0; y < CHIP8_SCREEN_HEIGHT; y++) {
            for(int x = 0; x < CHIP8_SCREEN_WIDTH; x++) {
                position.x = x * CHIP8_SCREEN_SCALE;
                position.y = y * CHIP8_SCREEN_SCALE;

                SDL_FillRect(SDL_GetWindowSurface(window), &position, (chip8.vram[y * CHIP8_SCREEN_WIDTH + x]) ? color_white : color_black);
            }
        }

        SDL_UpdateWindowSurface(window);

        // Do Chip8 CPU cycles
        Chip8_frame(&chip8);

        // Wait
        SDL_Delay(1000 / CHIP8_FRAMES_PER_SECOND);
    }

    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
