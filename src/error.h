#ifndef __ERROR_H__
#define __ERROR_H__

#define CHIP8_ERROR_UNKNOWN_OPCODE      0
#define CHIP8_ERROR_STACK_UNDERFLOW     1
#define CHIP8_ERROR_STACK_OVERFLOW      2

void die(const Chip8* chip8, const int errnum);

#endif
