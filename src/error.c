#include <stdio.h>
#include <stdlib.h>

#include "chip8.h"
#include "error.h"

const char* errstr[] = {
    "Unknown opcode",
    "Stack underflow",
    "Stack overflow"
};

void die(const Chip8* chip8, const int errnum) {
    // Print error string
    fprintf(stderr, errstr[errnum]);

    // Print machine state
    fprintf(stderr, "\n\nMachine state:\n");
    
    // Print Vx registers
    for(int i = 0; i < 16; i++) {
        fprintf(stderr, "V%01X=0x%02X\n", i, chip8->v[i]);
    }

    // print I register
    fprintf(stderr, "I=0x%03X\n", chip8->i);

    // Print timing registers
    fprintf(stderr, "DT=0x%02X\n", chip8->dt);
    fprintf(stderr, "ST=0x%02X\n", chip8->st);

    // Print keyboard state
    for(int i = 0; i < 16; i++) {
        fprintf(stderr, "Keyboard key %01X is %s\n", i, chip8->keyboard[i] ? "pressed" : "released");
    }

    // Print program counter value
    fprintf(stderr, "PC=0x%03X\n", chip8->pc);
    
    // Print stack pointer value
    fprintf(stderr, "SP=0x%01X\n", chip8->sp);

    // Print RAM dump
    fprintf(stderr, "\nRAM dump:\n");
    for(int y = 0; y < CHIP8_RAM_SIZE / 16; y++) {
        for(int x = 0; x < 16; x++) {
            fprintf(stderr, "%02X", chip8->ram[(y * 16) + x]);
        
            if(x < 15) {
                fputc(' ', stderr);
            }
        }

        fputc('\n', stderr);
    }

    // Print VRAM dump
    fprintf(stderr, "\nVRAM dump:\n");
    for(int y = 0; y < CHIP8_SCREEN_HEIGHT; y++) {
        for(int x = 0; x < CHIP8_SCREEN_WIDTH; x++) {
            fputc(chip8->vram[y * CHIP8_SCREEN_WIDTH + x] ? '#' : ' ', stderr);
        }

        fputc('\n', stderr);
    }

    // Print stack dump
    fprintf(stderr, "\nStack dump:\n");
    for(int i = 0; i < CHIP8_STACK_SIZE; i++) {
        fprintf(stderr, "stack[0x%01X]=0x%03X\n", i, chip8->stack[i]);
    }

    exit(1);
}
