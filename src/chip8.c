#include <stdio.h>
#include <stdlib.h>

#include "chip8.h"
#include "error.h"

SDL_Scancode Chip8_keymap[16] = {
    SDL_SCANCODE_X, SDL_SCANCODE_1, SDL_SCANCODE_2, SDL_SCANCODE_3,
    SDL_SCANCODE_Q, SDL_SCANCODE_W, SDL_SCANCODE_E, SDL_SCANCODE_A,
    SDL_SCANCODE_S, SDL_SCANCODE_D, SDL_SCANCODE_Z, SDL_SCANCODE_C,
    SDL_SCANCODE_4, SDL_SCANCODE_R, SDL_SCANCODE_F, SDL_SCANCODE_V
};

uint8_t Chip8_fontrom[16 * CHIP8_FONT_CHARACTER_HEIGHT] = {
    // 0
    0b11110000,
    0b10010000,
    0b10010000,
    0b10010000,
    0b11110000,
    
    // 1
    0b00100000,
    0b01100000,
    0b00100000,
    0b00100000,
    0b11110000,

    // 2
    0b11110000,
    0b00010000,
    0b11110000,
    0b10000000,
    0b11110000,

    // 3
    0b11110000,
    0b00010000,
    0b11110000,
    0b00010000,
    0b11110000,

    // 4
    0b10010000,
    0b10010000,
    0b11110000,
    0b00010000,
    0b00010000,

    // 5
    0b11110000,
    0b10000000,
    0b11110000,
    0b00010000,
    0b11110000,

    // 6
    0b11110000,
    0b10000000,
    0b11110000,
    0b10010000,
    0b11110000,

    // 7
    0b11110000,
    0b00010000,
    0b00100000,
    0b00100000,
    0b00100000,

    // 8
    0b11110000,
    0b10010000,
    0b11110000,
    0b10010000,
    0b11110000,

    // 9
    0b11110000,
    0b10010000,
    0b11110000,
    0b00010000,
    0b11110000,

    // A
    0b11110000,
    0b10010000,
    0b11110000,
    0b10010000,
    0b10010000,

    // B
    0b11100000,
    0b10010000,
    0b11100000,
    0b10010000,
    0b11100000,

    // C
    0b11110000,
    0b10000000,
    0b10000000,
    0b10000000,
    0b11110000,

    // D
    0b11100000,
    0b10010000,
    0b10010000,
    0b10010000,
    0b11100000,

    // E
    0b11110000,
    0b10000000,
    0b11110000,
    0b10000000,
    0b11110000,

    // F
    0b11110000,
    0b10000000,
    0b11110000,
    0b10000000,
    0b10000000
};

void Chip8_init(Chip8* chip8) {
    // Init Vx registers
    for(int i = 0; i < 16; i++) {
        chip8->v[i] = 0;
    }

    // Init other registers
    chip8->i = 0;
    chip8->st = 0;
    chip8->dt = 0;

    // Init keyboard
    for(int i = 0; i < 16; i++) {
        chip8->keyboard[i] = false;
    }

    // Init RAM
    for(int i = 0; i < CHIP8_RAM_SIZE; i++) {
        chip8->ram[i] = 0;
    }

    // Copy font ROM
    for(int i = 0; i < 16 * CHIP8_FONT_CHARACTER_HEIGHT; i++) {
        chip8->ram[CHIP8_FONT_ROM_ADDRESS + i] = Chip8_fontrom[i];
    }

    // Init VRAM
    for(int i = 0; i < CHIP8_VRAM_SIZE; i++) {
        chip8->vram[i] = false;
    }

    // Init program counter (PC)
    chip8->pc = CHIP8_LOAD_ADDRESS;

    // Init stack
    chip8->sp = CHIP8_STACK_SIZE - 1;
}

void Chip8_frame(Chip8* chip8) {
    // Do CPU cycles
    for(int i = 0; i < CHIP8_CYCLES_PER_FRAME; i++) {
        Chip8_cycle(chip8);
    }

    // Update sound timer register
    if(chip8->st > 0) {
        chip8->st--;

        if(chip8->st == 0) {
            // Beep
        }
    }

    // Update delay timer register
    if(chip8->dt > 0) {
        chip8->dt--;
    }

    // TODO: Update keyboard
    // TODO: Update graphics
}

void Chip8_cycle(Chip8* chip8) {
    // Fetch opcode
    uint16_t opcode = (chip8->ram[chip8->pc] << 8);

    // Particular case if PC = CHIP8_RAM_SIZE - 1, we fetch the 2nd byte of the
    // opcode at address 0 in RAM
    if(chip8->pc == CHIP8_RAM_SIZE - 1) {
        opcode += chip8->ram[0];
    } else {
        opcode += chip8->ram[chip8->pc + 1];
    }

    // Increment program counter
    chip8->pc += 2;

    // Make sure program counter will never point outside of RAM
    if(chip8->pc > CHIP8_RAM_SIZE) {
        chip8->pc %= CHIP8_RAM_SIZE;
    }

    // Variables used for interpreting
    uint16_t unsigned_result = 0;
    int16_t signed_result = 0;
    int key_index = 0;
    bool found = false;

    // Big instruction switch
    switch(CHIP8_OPCODE_A(opcode)) {
        case 0:
        switch(CHIP8_OPCODE_NNN(opcode)) {
            case 0x0e0:
            // CLS
            for(int i = 0; i < CHIP8_VRAM_SIZE; i++) {
                chip8->vram[i] = false;
            }
            break;

            case 0x0ee:
            // RET
            if(chip8->sp < CHIP8_STACK_SIZE - 1) {
                chip8->sp++;
                chip8->pc = chip8->stack[chip8->sp];
            } else {
                die(chip8, CHIP8_ERROR_STACK_UNDERFLOW);
            }
            break;

            default:
            die(chip8, CHIP8_ERROR_UNKNOWN_OPCODE);
            break;
        }
        break;

        case 1:
        // JP nnn
        chip8->pc = CHIP8_OPCODE_NNN(opcode);
        break;

        case 2:
        // CALL nnn
        if(chip8->sp > 0) {
            chip8->stack[chip8->sp--] = chip8->pc;
        } else {
            die(chip8, CHIP8_ERROR_STACK_OVERFLOW);
        }
        chip8->pc = CHIP8_OPCODE_NNN(opcode);
        break;

        case 3:
        // SE Vx, nn
        if(chip8->v[CHIP8_OPCODE_X(opcode)] == CHIP8_OPCODE_NN(opcode)) {
            // Increment program counter
            chip8->pc += 2;

            // Make sure program counter will never point outside of RAM
            if(chip8->pc > CHIP8_RAM_SIZE) {
                chip8->pc %= CHIP8_RAM_SIZE;
            }
        }
        break;

        case 4:
        // SNE Vx, nn
        if(chip8->v[CHIP8_OPCODE_X(opcode)] != CHIP8_OPCODE_NN(opcode)) {
            // Increment program counter
            chip8->pc += 2;

            // Make sure program counter will never point outside of RAM
            if(chip8->pc > CHIP8_RAM_SIZE) {
                chip8->pc %= CHIP8_RAM_SIZE;
            }
        }
        break;

        case 5:
        // SE Vx, Vy
        if(chip8->v[CHIP8_OPCODE_X(opcode)] == chip8->v[CHIP8_OPCODE_Y(opcode)]) {
            // Increment program counter
            chip8->pc += 2;

            // Make sure program counter will never point outside of RAM
            if(chip8->pc > CHIP8_RAM_SIZE) {
                chip8->pc %= CHIP8_RAM_SIZE;
            }
        }
        break;

        case 6:
        // LD Vx, nn
        chip8->v[CHIP8_OPCODE_X(opcode)] = CHIP8_OPCODE_NN(opcode);
        break;

        case 7:
        // ADD Vx, nn
        unsigned_result = chip8->v[CHIP8_OPCODE_X(opcode)] + CHIP8_OPCODE_NN(opcode);
        
        // Check if the result has overflown
        if(unsigned_result >= 0x100) {
            unsigned_result %= 0x100;
        }

        // Save the result in the register
        chip8->v[CHIP8_OPCODE_X(opcode)] = (uint8_t)unsigned_result;
        break;

        case 8:
        switch(CHIP8_OPCODE_N(opcode)) {
            case 0:
            // LD Vx, Vy
            chip8->v[CHIP8_OPCODE_X(opcode)] = chip8->v[CHIP8_OPCODE_Y(opcode)];
            break;

            case 1:
            // OR Vx, Vy
            chip8->v[CHIP8_OPCODE_X(opcode)] |= chip8->v[CHIP8_OPCODE_Y(opcode)];
            break;

            case 2:
            // AND Vx, Vy
            chip8->v[CHIP8_OPCODE_X(opcode)] &= chip8->v[CHIP8_OPCODE_Y(opcode)];
            break;

            case 3:
            // XOR Vx, Vy
            chip8->v[CHIP8_OPCODE_X(opcode)] ^= chip8->v[CHIP8_OPCODE_Y(opcode)];
            break;

            case 4:
            // ADD Vx, Vy
            // Calculate the result
            unsigned_result = chip8->v[CHIP8_OPCODE_X(opcode)] + chip8->v[CHIP8_OPCODE_Y(opcode)];

            // Set VF
            chip8->v[0xf] = (uint8_t)((unsigned_result & 0x0100) >> 8);

            // Set Vx to the result
            chip8->v[CHIP8_OPCODE_X(opcode)] = (uint8_t)unsigned_result;
            break;

            case 5:
            // SUB Vx, Vy
            signed_result = chip8->v[CHIP8_OPCODE_X(opcode)] - chip8->v[CHIP8_OPCODE_Y(opcode)];
            chip8->v[0xf] = (signed_result >= 0);
            chip8->v[CHIP8_OPCODE_X(opcode)] = (uint8_t)signed_result;
            break;

            case 6:
            // SHR Vx
            chip8->v[0xf] = chip8->v[CHIP8_OPCODE_X(opcode)] & 0x01;
            chip8->v[CHIP8_OPCODE_X(opcode)] >>= 1;
            break;

            case 7:
            // SUBN Vx, Vy
            signed_result = chip8->v[CHIP8_OPCODE_Y(opcode)] - chip8->v[CHIP8_OPCODE_X(opcode)];
            chip8->v[0xf] = (signed_result >= 0);
            chip8->v[CHIP8_OPCODE_X(opcode)] = (uint8_t)signed_result;
            break;

            case 0xe:
            // SHL Vx
            chip8->v[0xf] = (chip8->v[CHIP8_OPCODE_X(opcode)] >> 7) & 0x01;
            chip8->v[CHIP8_OPCODE_X(opcode)] = (chip8->v[CHIP8_OPCODE_X(opcode)] << 1) & 0xff;
            break;

            default:
            die(chip8, CHIP8_ERROR_UNKNOWN_OPCODE);
            break;
        }
        break;

        case 9:
        // SNE Vx, Vy
        if(chip8->v[CHIP8_OPCODE_X(opcode)] != chip8->v[CHIP8_OPCODE_Y(opcode)]) {
            // Increment program counter
            chip8->pc += 2;

            // Make sure program counter will never point outside of RAM
            if(chip8->pc > CHIP8_RAM_SIZE) {
                chip8->pc %= CHIP8_RAM_SIZE;
            }
        }

        case 0xa:
        // LD I, nnn
        chip8->i = CHIP8_OPCODE_NNN(opcode);
        break;

        case 0xb:
        // JP V0, nnn
        unsigned_result = chip8->v[0] + CHIP8_OPCODE_NNN(opcode);

        // Make sure the resulting address is not pointing outside of RAM
        if(unsigned_result >= CHIP8_RAM_SIZE) {
            unsigned_result %= CHIP8_RAM_SIZE;
        }

        // Jump
        chip8->pc = unsigned_result;
        break;

        case 0xc:
        // RND Vx, nn
        chip8->v[CHIP8_OPCODE_X(opcode)] = (uint8_t)(rand() & CHIP8_OPCODE_NN(opcode));
        break;

        case 0xd:
        // DRW Vx, Vy, n
        chip8->v[0xf] = 0;

        for(int y = 0; y < CHIP8_OPCODE_N(opcode); y++) {
            // Calculate sprite line address
            unsigned_result = chip8->i + y;

            // Make sure this address is pointing somewhere in RAM
            if(unsigned_result >= CHIP8_RAM_SIZE) {
                unsigned_result %= CHIP8_RAM_SIZE;
            }
            
            // Read this address
            uint8_t spriteLine = chip8->ram[unsigned_result];

            // Draw pixel line
            for(int x = 0; x < 8; x++) {
                // Calculate VRAM address
                unsigned_result = ((chip8->v[CHIP8_OPCODE_Y(opcode)] + y) * CHIP8_SCREEN_WIDTH) + chip8->v[CHIP8_OPCODE_X(opcode)] + x;

                // Make sure this address is pointing to somewhere in VRAM
                if(unsigned_result >= CHIP8_VRAM_SIZE) {
                    unsigned_result %= CHIP8_VRAM_SIZE;
                }

                // Read this address
                bool screenPixel = chip8->vram[unsigned_result];

                // Read sprite pixel
                bool spritePixel = (((spriteLine >> (7 - x)) & 0x01) != 0);

                // XOR screen pixel
                chip8->vram[unsigned_result] ^= spritePixel;

                // Set VF to 1 on sprite collision
                if(screenPixel && spritePixel) {
                    chip8->v[0xf] = 1;
                }
            }
        }
        break;

        case 0xe:
        switch(CHIP8_OPCODE_NN(opcode)) {
            case 0x9e:
            // SKP Vx
            key_index = chip8->v[CHIP8_OPCODE_X(opcode)];

            if(key_index >= 16) {
                fprintf(stderr, "Warning: key index not in range.\n");
                key_index &= 0x0f;
            }

            if(chip8->keyboard[key_index]) {
                // Increment program counter
                chip8->pc += 2;

                // Make sure program counter will never point outside of RAM
                if(chip8->pc > CHIP8_RAM_SIZE) {
                    chip8->pc %= CHIP8_RAM_SIZE;
                }
            }
            break;

            case 0xa1:
            // SKP Vx
            key_index = chip8->v[CHIP8_OPCODE_X(opcode)];

            if(key_index >= 16) {
                fprintf(stderr, "Warning: key index not in range.\n");
                key_index &= 0x0f;
            }

            if(!chip8->keyboard[key_index]) {
                // Increment program counter
                chip8->pc += 2;

                // Make sure program counter will never point outside of RAM
                if(chip8->pc > CHIP8_RAM_SIZE) {
                    chip8->pc %= CHIP8_RAM_SIZE;
                }
            }
            break;

            default:
            die(chip8, CHIP8_ERROR_UNKNOWN_OPCODE);
            break;
        }
        break;

        case 0xf:
        switch(CHIP8_OPCODE_NN(opcode)) {
            case 0x07:
            // LD Vx, DT
            chip8->v[CHIP8_OPCODE_X(opcode)] = chip8->dt;
            break;

            case 0x0a:
            // LD Vx, K
            for(int i = 0; i < 16; i++) {
                if(chip8->keyboard[i]) {
                    chip8->v[CHIP8_OPCODE_X(opcode)] = i;
                    found = true;
                    break;
                }
            }

            if(!found) {
                // Decrement PC
                signed_result = chip8->pc;
                signed_result -= 2;

                // Make sure PC never points outside of RAM
                if(signed_result < 0) {
                    signed_result += CHIP8_RAM_SIZE;
                }

                chip8->pc = (uint16_t)signed_result;
            }
            break;

            case 0x15:
            // LD DT, Vx
            chip8->dt = chip8->v[CHIP8_OPCODE_X(opcode)];
            break;

            case 0x18:
            // LD ST, Vx
            chip8->st = chip8->v[CHIP8_OPCODE_X(opcode)];
            break;

            case 0x1e:
            // ADD I, Vx
            chip8->i += chip8->v[CHIP8_OPCODE_X(opcode)];
            break;

            case 0x29:
            // LD F, Vx
            unsigned_result = chip8->v[CHIP8_OPCODE_X(opcode)];

            if(unsigned_result > 16) {
                fprintf(stderr, "Warning: character number >= 16\n");
                unsigned_result &= 0x0f;
            }

            chip8->i = CHIP8_FONT_ROM_ADDRESS + (unsigned_result * CHIP8_FONT_CHARACTER_HEIGHT);
            break;

            case 0x33:
            // LD B, Vx
            unsigned_result = chip8->i;

            // Make sure to point to an address in RAM
            if(unsigned_result >= CHIP8_RAM_SIZE) {
                unsigned_result %= CHIP8_RAM_SIZE;
            }

            // Store first digit at address I
            chip8->ram[unsigned_result] = chip8->v[CHIP8_OPCODE_X(opcode)] / 100;

            // Increment address
            unsigned_result++;

            // Make sure to point to an address in RAM
            if(unsigned_result >= CHIP8_RAM_SIZE) {
                unsigned_result %= CHIP8_RAM_SIZE;
            }

            // Store second digit at address I + 1
            chip8->ram[unsigned_result] = (chip8->v[CHIP8_OPCODE_X(opcode)] / 10) % 10;

            // Increment address
            unsigned_result++;

            // Make sure to point to an address in RAM
            if(unsigned_result >= CHIP8_RAM_SIZE) {
                unsigned_result %= CHIP8_RAM_SIZE;
            }

            // Store third digit at address I + 2
            chip8->ram[unsigned_result] = chip8->v[CHIP8_OPCODE_X(opcode)] % 10;
            break;

            case 0x55:
            // LD [I], Vx
            for(int i = 0; i <= CHIP8_OPCODE_X(opcode); i++) {
                chip8->ram[(chip8->i + i) % CHIP8_RAM_SIZE] = chip8->v[i];
            }
            break;

            case 0x65:
            // LD Vx, [I]
            for(int i = 0; i <= CHIP8_OPCODE_X(opcode); i++) {
                chip8->v[i] = chip8->ram[(chip8->i + i) % CHIP8_RAM_SIZE];
            }
            break;

            default:
            die(chip8, CHIP8_ERROR_UNKNOWN_OPCODE);
            break;
        }
        break;
    }
}
