CC=gcc -c
LD=gcc
CFLAGS=-Wall -Wextra -march=native -O2 `sdl2-config --cflags` -std=gnu99 -s
LDFLAGS=`sdl2-config --libs` -s
SOURCES=$(wildcard src/*.c)
OBJECTS=$(SOURCES:.c=.o)

all: bin/c8emu

bin/c8emu: $(OBJECTS)
	if [ ! -d bin ]; then \
		mkdir bin; \
	fi
	$(LD) $(LDFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJECTS)
	rm -f bin/c8emu
	rm -rf bin
